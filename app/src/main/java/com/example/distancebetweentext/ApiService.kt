package com.example.distancebetweentext

import retrofit2.Call
import retrofit2.http.GET

interface ApiService {
    @GET("XMLReleaseALL_public/scenic_spot_C_f.json")
    fun getData(): Call<dataModel>
}
