package com.example.distancebetweentext

import android.graphics.PixelFormat
import android.graphics.Rect
import android.os.Bundle
import android.view.LayoutInflater
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.WindowCompat
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.WindowInsetsControllerCompat
import androidx.viewbinding.ViewBinding


/****************************************************
 * Author: AlanLai
 * Create Date: 2020/4/24
 * Usage:
 *
 * Revision History
 * Date         Author           Description
 ****************************************************/

abstract class BaseActivity<VB : ViewBinding> : AppCompatActivity() {

    private var _binding: ViewBinding? = null
    abstract val mBindingInflater: (LayoutInflater) -> VB

    @Suppress("UNCHECKED_CAST")
    protected val mBinding: VB
        get() = _binding as VB


    abstract fun initConfiguration()

    abstract fun initListener()

    abstract fun observeLiveData()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding = mBindingInflater.invoke(layoutInflater)
        setContentView(requireNotNull(_binding).root)
        window.setFormat(PixelFormat.TRANSLUCENT)
        initConfiguration()
        initListener()
        observeLiveData()


        mBinding.root.viewTreeObserver.addOnGlobalLayoutListener {
            val rect = Rect()
            this.window.decorView.getWindowVisibleDisplayFrame(rect)
            val screenHeight: Int = window.decorView.rootView.height
            val heightDifference: Int = screenHeight - rect.bottom
            if (heightDifference > 200) {
                showSystemUI()
            } else {
                hideSystemUI()
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    private fun hideSystemUI() {
        WindowCompat.setDecorFitsSystemWindows(window, false)
        WindowInsetsControllerCompat(window, mBinding.root).let { controller ->
            controller.hide(WindowInsetsCompat.Type.navigationBars())
            controller.systemBarsBehavior = WindowInsetsControllerCompat.BEHAVIOR_SHOW_TRANSIENT_BARS_BY_SWIPE
        }
    }

    private fun showSystemUI() {
        WindowCompat.setDecorFitsSystemWindows(window, false)
        WindowInsetsControllerCompat(window, mBinding.root).show(WindowInsetsCompat.Type.navigationBars())
    }


}
