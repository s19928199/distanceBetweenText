package com.example.distancebetweentext
import com.google.gson.annotations.SerializedName


/****************************************************
 * Author: alanlai
 * Create Date: 2021/10/18
 * Usage:
 *
 * Revision History
 * Date         Author           Description
 ****************************************************/

data class dataModel(
    @SerializedName("XML_Head")
    val mXMLHead: XMLHead
) {
    data class XMLHead(
        @SerializedName("Infos")
        val mInfos: Infos,
        @SerializedName("Language")
        val mLanguage: String,
        @SerializedName("Listname")
        val mListname: String,
        @SerializedName("Orgname")
        val mOrgname: String,
        @SerializedName("Updatetime")
        val mUpdatetime: String
    ) {
        data class Infos(
            @SerializedName("Info")
            val mInfo: MutableList<Info>
        ) {
            data class Info(
                @SerializedName("Add")
                val mAdd: String,
                @SerializedName("Changetime")
                val mChangetime: String,
                @SerializedName("Class1")
                val mClass1: String,
                @SerializedName("Class2")
                val mClass2: String,
                @SerializedName("Class3")
                val mClass3: String,
                @SerializedName("Description")
                val mDescription: String,
                @SerializedName("Gov")
                val mGov: String,
                @SerializedName("Id")
                val mId: String,
                @SerializedName("Keyword")
                val mKeyword: String,
                @SerializedName("Level")
                val mLevel: String,
                @SerializedName("Map")
                val mMap: String,
                @SerializedName("Name")
                val mName: String,
                @SerializedName("Opentime")
                val mOpentime: String,
                @SerializedName("Orgclass")
                val mOrgclass: String,
                @SerializedName("Parkinginfo")
                val mParkinginfo: String,
                @SerializedName("Parkinginfo_Px")
                val mParkinginfoPx: Double,
                @SerializedName("Parkinginfo_Py")
                val mParkinginfoPy: Double,
                @SerializedName("Picdescribe1")
                val mPicdescribe1: String,
                @SerializedName("Picdescribe2")
                val mPicdescribe2: String,
                @SerializedName("Picdescribe3")
                val mPicdescribe3: String,
                @SerializedName("Picture1")
                val mPicture1: String,
                @SerializedName("Picture2")
                val mPicture2: String,
                @SerializedName("Picture3")
                val mPicture3: String,
                @SerializedName("Px")
                val mPx: Double,
                @SerializedName("Py")
                val mPy: Double,
                @SerializedName("Region")
                val mRegion: String,
                @SerializedName("Remarks")
                val mRemarks: String,
                @SerializedName("Tel")
                val mTel: String,
                @SerializedName("Ticketinfo")
                val mTicketinfo: String,
                @SerializedName("Toldescribe")
                val mToldescribe: String,
                @SerializedName("Town")
                val mTown: String,
                @SerializedName("Travellinginfo")
                val mTravellinginfo: String,
                @SerializedName("Website")
                val mWebsite: String,
                @SerializedName("Zipcode")
                val mZipcode: String,
                @SerializedName("Zone")
                val mZone: String
            )
        }
    }
}