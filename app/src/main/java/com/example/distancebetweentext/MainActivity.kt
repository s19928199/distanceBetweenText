package com.example.distancebetweentext

import android.Manifest
import android.annotation.SuppressLint
import android.content.Intent
import android.location.Address
import android.location.Geocoder
import android.media.ExifInterface
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.inputmethod.EditorInfo
import android.widget.Toast
import androidx.activity.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.GridLayoutManager
import com.example.distancebetweentext.databinding.ActivityMainBinding
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import pub.devrel.easypermissions.EasyPermissions
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.File
import java.io.IOException
import java.io.InputStream
import java.io.Serializable
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.collections.ArrayList
import kotlin.collections.HashMap


class MainActivity : BaseActivity<ActivityMainBinding>(), EasyPermissions.PermissionCallbacks {
    override val mBindingInflater: (LayoutInflater) -> ActivityMainBinding =
        ActivityMainBinding::inflate
    private val mViewModel: MainViewModel by viewModels()

    //需要的權限
    private val mPermissions = arrayOf(
        Manifest.permission.ACCESS_MEDIA_LOCATION,
        Manifest.permission.READ_EXTERNAL_STORAGE,
        Manifest.permission.WRITE_EXTERNAL_STORAGE
    )

    private val mAdapter by lazy {
        MainAdapter(onItemClick = {
            startActivity(Intent(this, PhotoActivity::class.java).apply {
                putExtras(Bundle().apply {
                    putSerializable("PictureAttractionsBean", it as Serializable)
                })
            })
        })
    }

    override fun initConfiguration() {
        if (!EasyPermissions.hasPermissions(this, *mPermissions)) {
            EasyPermissions.requestPermissions(
                this,
                "請求權限",
                0,
                *mPermissions
            )
        } else {
            request()
        }

        mBinding.recyclerView.layoutManager = GridLayoutManager(this, 3)
        mBinding.recyclerView.addItemDecoration(
            GridSpacingItemDecoration(
                3,
                resources.getDimensionPixelSize(R.dimen.dp_5),
                true
            )
        )
        mBinding.recyclerView.adapter = mAdapter
    }

    override fun initListener() {
        mBinding.editText.setOnEditorActionListener { v, actionId, event ->
            when (actionId) {
                EditorInfo.IME_ACTION_SEARCH -> {
                    mViewModel.matchData.value?.let {
                        mAdapter.setDataList(it.groupBy { bean ->
                            bean.attractionsName
                        }.filter { entry ->
                            entry.key.contains(mBinding.editText.text)
                        }.toList().toMutableList())
                    }
                }
            }
            return@setOnEditorActionListener true
        }
    }

    override fun observeLiveData() {
        mViewModel.attractionsData.observe(this, Observer {
            scan()
        })

        mViewModel.photoData.observe(this, Observer {
            val matchDataList = mutableListOf<PictureAttractionsBean>()
            ToastUtil.showToast(this@MainActivity, "開始比對手機照片", Toast.LENGTH_SHORT)
            it.forEach { photo ->
                val attractionsDistanceList = mutableListOf<Pair<String, Double>>()
                mViewModel.attractionsData.value?.forEach { attractions ->
                    photo.longitude?.toDouble()?.let { it1 ->
                        photo.latitude?.toDouble()?.let { it2 ->
                            attractionsDistanceList.add(
                                Pair(
                                    attractions.mName,
                                    GPSHelper.calculateDistance(
                                        it1,
                                        it2,
                                        attractions.mPx,
                                        attractions.mPy,
                                    )
                                )
                            )
                        }
                    }
                }

                val minData = attractionsDistanceList.minByOrNull { pair ->
                    pair.second
                }

                minData?.second?.toInt()?.let {
                    if (minData.second.toInt() <= 1000) {
                        matchDataList.add(
                            PictureAttractionsBean(
                                mediaBean = photo,
                                attractionsName = minData.first,
                                attractionsDistance = minData.second.toInt()
                            )
                        )
                    } else {
                        val geocoder = Geocoder(this, Locale.TRADITIONAL_CHINESE)
                        var lstAddress = mutableListOf<Address>()
                        photo.latitude?.toDouble()?.let { lat ->
                            photo.longitude?.toDouble()?.let { lng ->
                                lstAddress = geocoder.getFromLocation(lat, lng, 1);
                            }
                        }

                        try {
                            matchDataList.add(
                                PictureAttractionsBean(
                                    mediaBean = photo,
                                    attractionsName = lstAddress[0].locality,
                                    attractionsDistance = 0
                                )
                            )
                        } catch (e: Exception) {

                        }
                    }
                }
            }
            mViewModel.matchData.postValue(matchDataList)
        })

        mViewModel.matchData.observe(this, Observer {
            mAdapter.setDataList(it.groupBy { bean ->
                bean.attractionsName
            }.toList().toMutableList())
        })
    }

    private fun request() {
        val okHttpClient = OkHttpClient.Builder()
            .addInterceptor(Interceptor { chain ->
                val newRequest = chain.request().newBuilder()
                    .addHeader("Content-Type", "application/json")
                    .url(chain.request().url)
                    .build()

                chain.proceed(newRequest)
            })
            .connectTimeout(60, TimeUnit.SECONDS)
            .readTimeout(60, TimeUnit.SECONDS)
            .writeTimeout(60, TimeUnit.SECONDS)
            .build()


        val retrofit = Retrofit.Builder()
            .baseUrl("http://gis.taiwan.net.tw/") // 設置 網絡請求 Url
            .addConverterFactory(GsonConverterFactory.create()) //設置使用Gson解析(記得加入依賴)
            .client(okHttpClient)
            .build()
        val request: ApiService = retrofit.create(ApiService::class.java)
        val call: Call<dataModel> = request.getData()
        call.enqueue(object : Callback<dataModel> {
            override fun onResponse(call: Call<dataModel>, response: Response<dataModel>) {
                ToastUtil.showToast(this@MainActivity, "取得景點資料庫", Toast.LENGTH_SHORT)
                mViewModel.attractionsData.postValue(response.body()?.mXMLHead?.mInfos?.mInfo!!)
            }

            override fun onFailure(call: Call<dataModel>, t: Throwable) {
            }
        })
    }

    @SuppressLint("Range")
    private fun scan() {
        lifecycleScope.launch(Dispatchers.IO) {
            val mediaBeen: MutableList<MediaBean> = ArrayList()
            val allPhotosTemp: HashMap<String, MutableList<MediaBean>> = HashMap() //所有照片

            val mCursor = contentResolver.query(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                null,
                null,
                null,
                null
            )

            if (null != mCursor && mCursor.moveToFirst()) {
                do {
                    val id = mCursor.getLong(mCursor.getColumnIndex(MediaStore.Images.Media._ID))
                    val path =
                        mCursor.getString(mCursor.getColumnIndex(MediaStore.Images.Media.DATA))
                    var displayName =
                        mCursor.getString(mCursor.getColumnIndex(MediaStore.Images.Media.DISPLAY_NAME))
                    val size =
                        mCursor.getInt(mCursor.getColumnIndex(MediaStore.Images.Media.SIZE)) / 1024

                    var uri = Uri.withAppendedPath(
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                        mCursor.getString(mCursor.getColumnIndexOrThrow(MediaStore.Images.ImageColumns._ID))
                    )

                    // check path
                    if (TextUtils.isEmpty(path)) {
                        continue
                    }

                    // filter out files from the MediaStore that have no entities
                    if (size <= 0) {
                        continue
                    }

                    // get name
                    if (TextUtils.isEmpty(displayName)) {
                        displayName = path.substring(path.lastIndexOf("/") + 1)
                    }

                    var exif: ExifInterface? = null
                    var latitude: String? = null
                    var longitude: String? = null
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {

                        try {
                            uri = MediaStore.setRequireOriginal(uri)

                            val inputStream: InputStream? = contentResolver.openInputStream(uri)
                            inputStream?.run {
                                exif = ExifInterface(this)
                            }

                            latitude = exif?.getAttribute(ExifInterface.TAG_GPS_LATITUDE)
                            longitude = exif?.getAttribute(ExifInterface.TAG_GPS_LONGITUDE)

                            if (!TextUtils.isEmpty(latitude) && !TextUtils.isEmpty(
                                    longitude
                                )
                            ) {
                                latitude = latitude!!.replace("/", ",")
                                longitude = longitude!!.replace("/", ",")
                                val lat = latitude.split(",".toRegex()).toTypedArray()
                                val lng = longitude.split(",".toRegex()).toTypedArray()
                                var latD = 0f
                                var latM = 0f
                                var latS = 0f
                                var lngD = 0f
                                var lngM = 0f
                                var lngS = 0f
                                if (lat.size >= 2) {
                                    latD = lat[0].toFloat() / lat[1].toFloat()
                                }
                                if (lat.size >= 4) {
                                    latM = lat[2].toFloat() / lat[3].toFloat()
                                }
                                if (lat.size >= 6) {
                                    latS = lat[4].toFloat() / lat[5].toFloat()
                                }
                                if (lng.size >= 1) {
                                    lngD = lng[0].toFloat() / lng[1].toFloat()
                                }
                                if (lng.size >= 2) {
                                    lngM = lng[2].toFloat() / lng[3].toFloat()
                                }
                                if (lng.size >= 3) {
                                    lngS = lng[4].toFloat() / lng[5].toFloat()
                                }
                                latitude = (latD + latM / 60 + latS / 3600).toString()
                                longitude = (lngD + lngM / 60 + lngS / 3600).toString()
                            }

                        } catch (e: IOException) {
                            e.printStackTrace()
                        }
                    }

                    //用於展示相簿初始化介面
                    mediaBeen.add(
                        MediaBean(
                            MediaBean.Type.Image,
                            path,
                            size,
                            displayName,
                            latitude,
                            longitude
                        )
                    )
                    // 獲取該圖片的父路徑名
                    val dirPath: String = File(path).parentFile.absolutePath
                    //儲存對應關係
                    if (allPhotosTemp.containsKey(dirPath)) {
                        val data: MutableList<MediaBean> = allPhotosTemp[dirPath]!!
                        data.add(
                            MediaBean(
                                MediaBean.Type.Image,
                                path,
                                size,
                                displayName,
                                latitude,
                                longitude
                            )
                        )
                        continue
                    } else {
                        val data: MutableList<MediaBean> = ArrayList()
                        data.add(
                            MediaBean(
                                MediaBean.Type.Image,
                                path,
                                size,
                                displayName,
                                latitude,
                                longitude
                            )
                        )
                        allPhotosTemp[dirPath] = data
                    }
                } while (mCursor.moveToNext())
                mCursor.close()
                withContext(Dispatchers.Main) {
                    //更新介面
                    Log.e("TAG", "key = ${allPhotosTemp.keys}")
                    val list = mutableListOf<MainActivity.MediaBean>()

                    for (key in allPhotosTemp.keys) {
                        allPhotosTemp[key]?.let { list.addAll(it) }
                    }

                    val map = allPhotosTemp[allPhotosTemp.keys.find {
                        it.contains("Camera")
                    }]
                    mViewModel.photoData.postValue(map)
                }
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String?>,
        grantResults: IntArray,
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        EasyPermissions.onRequestPermissionsResult(
            requestCode,
            permissions,
            grantResults,
            this
        )
    }

    override fun onPermissionsGranted(requestCode: Int, perms: MutableList<String>) {
        request()
    }

    override fun onPermissionsDenied(requestCode: Int, perms: MutableList<String>) {
    }

    data class MediaBean(
        val type: Type,
        val path: String,
        val size: Int,
        val displayName: String,
        val latitude: String?,
        val longitude: String?
    ) : Serializable {
        enum class Type {
            Image, Video
        }
    }

    data class PictureAttractionsBean(
        val mediaBean: MediaBean,
        val attractionsName: String,
        val attractionsDistance: Int,
    ) : Serializable

}