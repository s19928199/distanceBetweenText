package com.example.distancebetweentext

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class MainViewModel : ViewModel() {
    val attractionsData = MutableLiveData<List<dataModel.XMLHead.Infos.Info>>()
    val photoData = MutableLiveData<List<MainActivity.MediaBean>>()
    val matchData = MutableLiveData<List<MainActivity.PictureAttractionsBean>>()
}