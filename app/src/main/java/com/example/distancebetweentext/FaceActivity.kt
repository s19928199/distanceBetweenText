package com.example.distancebetweentext

import android.view.LayoutInflater
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.example.distancebetweentext.databinding.ActivityFaceBinding
import java.io.File

class FaceActivity : BaseActivity<ActivityFaceBinding>() {
    override val mBindingInflater: (LayoutInflater) -> ActivityFaceBinding =
        ActivityFaceBinding::inflate

    private val mPictureAttractionsBean by lazy {
        intent.extras?.getSerializable("PictureAttractionsBean") as MainActivity.PictureAttractionsBean
    }


    override fun initConfiguration() {
        Glide.with(this)
            .load(mPictureAttractionsBean.mediaBean.path)
            .transform(CenterCrop(), RoundedCorners(25))
            .into(mBinding.imageView)

        FaceDetectionUtils.coverFaceWithSticker(this, File(mPictureAttractionsBean.mediaBean.path),
            smilingProbabilityCallback = {
                mBinding.TextView1.text = "smilingProbability = ${it.toString()}"
            },
            leftEyeOpenProbabilityCallback = {
                mBinding.TextView2.text = "leftEyeOpenProbability = ${it.toString()}"
            },
            rightEyeOpenProbabilityCallback = {
                mBinding.TextView3.text = "rightEyeOpenProbability = ${it.toString()}"
            })

    }

    override fun initListener() {
    }

    override fun observeLiveData() {
    }
}