package com.example.distancebetweentext

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import androidx.recyclerview.widget.GridLayoutManager
import com.example.distancebetweentext.databinding.ActivityPhotoBinding
import java.io.Serializable

class PhotoActivity : BaseActivity<ActivityPhotoBinding>() {
    override val mBindingInflater: (LayoutInflater) -> ActivityPhotoBinding =
        ActivityPhotoBinding::inflate

    private val mAdapter by lazy {
        PhotoAdapter(onItemClick = {
            startActivity(Intent(this, FaceActivity::class.java).apply {
                putExtras(Bundle().apply {
                    putSerializable("PictureAttractionsBean", it as Serializable)
                })
            })
        })
    }

    override fun initConfiguration() {
        intent.extras?.let {
            mAdapter.setDataList(it.getSerializable("PictureAttractionsBean") as MutableList<MainActivity.PictureAttractionsBean>)
        }


        mBinding.recyclerView.layoutManager = GridLayoutManager(this, 3)
        mBinding.recyclerView.addItemDecoration(
            GridSpacingItemDecoration(
                3,
                resources.getDimensionPixelSize(R.dimen.dp_5),
                true
            )
        )
        mBinding.recyclerView.adapter = mAdapter
    }

    override fun initListener() {
    }

    override fun observeLiveData() {
    }
}