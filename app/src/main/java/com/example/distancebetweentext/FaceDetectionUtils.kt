package com.example.distancebetweentext

import android.content.Context
import android.net.Uri
import android.util.Log
import com.google.mlkit.vision.common.InputImage
import com.google.mlkit.vision.face.FaceDetection
import com.google.mlkit.vision.face.FaceDetectorOptions
import java.io.File

class FaceDetectionUtils {

    companion object {
        private val options = FaceDetectorOptions.Builder()
            .setPerformanceMode(FaceDetectorOptions.PERFORMANCE_MODE_FAST)
            .setClassificationMode(FaceDetectorOptions.CLASSIFICATION_MODE_ALL)
            .setLandmarkMode(FaceDetectorOptions.LANDMARK_MODE_NONE)
            .setContourMode(FaceDetectorOptions.CONTOUR_MODE_NONE)
            .setMinFaceSize(20.0f)
            .enableTracking()
            .build()

        private val detector = FaceDetection.getClient(options)

        fun coverFaceWithSticker(
            context: Context,
            file: File,
            smilingProbabilityCallback: (faceCount: Float) -> Unit,
            leftEyeOpenProbabilityCallback: (faceCount: Float) -> Unit,
            rightEyeOpenProbabilityCallback: (faceCount: Float) -> Unit
        ) {

            val image = InputImage.fromFilePath(context, Uri.fromFile(file))
            detector.process(image)
                .addOnSuccessListener { faces ->
                    for (face in faces) {
                        Log.e("TAG","file = ${file.name}")
                        Log.e("TAG","face.smilingProbability = ${face.smilingProbability}")
                        Log.e("TAG","face.leftEyeOpenProbability = ${face.leftEyeOpenProbability}")
                        Log.e("TAG","face.rightEyeOpenProbability = ${face.rightEyeOpenProbability}")
                        smilingProbabilityCallback.invoke(face.smilingProbability)
                        leftEyeOpenProbabilityCallback.invoke(face.leftEyeOpenProbability)
                        rightEyeOpenProbabilityCallback.invoke(face.rightEyeOpenProbability)
                    }
                }
                .addOnFailureListener {
                    smilingProbabilityCallback.invoke(0f)
                    leftEyeOpenProbabilityCallback.invoke(0f)
                    rightEyeOpenProbabilityCallback.invoke(0f)
                }
        }

    }
}