package com.example.distancebetweentext

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners

/****************************************************************
 * Copyright (C) Kevin Corporation. All rights reserved.
 *
 * Author: Kevin Lin
 * Create Date: 2019/2/22
 * Usage:
 *
 * Revision History
 * Date         Author           Description
 */
class MainAdapter(
    private val onItemClick: (item: List<MainActivity.PictureAttractionsBean>) -> Unit
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private lateinit var mContext: Context
    private var mDataList = mutableListOf<Pair<String, List<MainActivity.PictureAttractionsBean>>>()

    fun setDataList(itemList: MutableList<Pair<String, List<MainActivity.PictureAttractionsBean>>>) {

        Log.e("TAG", "setDataList = ${itemList}")
        mDataList = itemList
        notifyDataSetChanged()
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layoutInflater: LayoutInflater = LayoutInflater.from(parent.context)
        mContext = parent.context
        return ItemViewHolder(
            layoutInflater.inflate(
                R.layout.adapter_main,
                parent, false
            )
        )
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        holder as ItemViewHolder
        holder.attractionsName.text =
            "${mDataList[position].first} ${"照片數量 = ${mDataList[position].second.size}"}"
        Glide.with(mContext)
            .load(mDataList[position].second[0].mediaBean.path)
            .transform(CenterCrop(), RoundedCorners(25))
            .into(holder.attractionsImage)

        holder.itemView.setOnClickListener {
            onItemClick.invoke(mDataList[position].second)
        }
    }

    override fun getItemCount(): Int {
        return mDataList.size
    }

    private inner class ItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val attractionsName: TextView = itemView.findViewById(R.id.adapter_name)
        val attractionsImage: ImageView = itemView.findViewById(R.id.adapter_imageView)
    }
}