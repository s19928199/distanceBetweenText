package com.example.distancebetweentext

import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity


class GetAllMsg : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_get_all_msg)
        val projection = arrayOf(
            MediaStore.Images.Media._ID,
            MediaStore.Images.Media.DISPLAY_NAME,
            MediaStore.Images.Media.DATA
        )
        val orderBy = MediaStore.Images.Media.DISPLAY_NAME
        val uri: Uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI
        getContentProvider(uri, projection, orderBy)

    }

    /**
     * 獲取ContentProvider
     *
     * @param projection
     * @param orderBy
     */
    fun getContentProvider(uri: Uri?, projection: Array<String>, orderBy: String?) {
        val cursor = uri?.let { this.contentResolver.query(it, projection, null, null, orderBy) } ?: return
        while (cursor.moveToNext()) {
            val map = HashMap<Any, Any>()
            for (i in projection.indices) {
                map[projection[i]] = cursor.getString(i)

                Log.e("tatat ","wkoewke =  ${projection[i]}:::::::${cursor.getString(i)}")
            }
        }
    }
}