package com.example.distancebetweentext

import android.location.Location
import java.util.*
import kotlin.math.roundToInt

/****************************************************
 * Author: alanlai
 * Create Date: 2021/10/19
 * Usage:
 *
 * Revision History
 * Date         Author           Description
 */
object GPSHelper {
    // 圆周率
    const val PI = 3.14159265358979324

    // 赤道半径(单位m)  
    private const val EARTH_RADIUS = 6378137.0

    /**
     * 转化为弧度(rad)
     */
    private fun rad(d: Double): Double {
        return d * Math.PI / 180.0
    }

    fun calculateDistance(lat1: Double, lng1: Double, lat2: Double, lng2: Double): Double {

        val selected_location = Location("locationA")
        selected_location.latitude = lat1
        selected_location.longitude = lng1
        val near_locations = Location("locationB")
        near_locations.latitude = lat2
        near_locations.longitude = lng2

        return selected_location.distanceTo(near_locations).toDouble()
    }

    private fun distanceText(distance: Float): String {
        return if (distance < 1000)
            if (distance < 1)
                String.format(Locale.US, "%dm", 1)
            else
                String.format(Locale.US, "%dm", distance.roundToInt())
        else if (distance > 10000)
//            if (distance < 1000000)
                String.format(Locale.US, "%dkm", (distance / 1000).roundToInt())
//            else
//                "FAR"
        else
            String.format(Locale.US, "%.2fkm", distance / 1000)
    }
}